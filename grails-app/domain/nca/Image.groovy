package nca

import groovy.transform.ToString
@ToString(includeNames = true)
class Image {
    String path
    String name

    static constraints = {
        path unique: true, blank: false
    }
    static mapping = {
        version(false)
    }
}
