package nca

import groovy.transform.ToString
@ToString(includeNames = true)
class Author {
    String name
    Image image
    Date birth

    static mapping = {
        version(false)
        table("Author")
    }
}
