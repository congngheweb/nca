package nca

import groovy.transform.ToString
@ToString(includeNames = true)
class Kind {
    String name

    static mapping = {
        version(false)
    }
}
