package nca

import groovy.transform.ToString
@ToString(includeNames = true)
class Singer {
    String name
    Date birth

    static belongsTo = [image:Image]

    static mapping = {
        version(false)
        table("Singer")
    }
}
