package nca

import groovy.transform.ToString
@ToString(includeNames = true)
class Album {
    String name
    Image image

    static belongsTo = [singer:Singer]
    static constraints = {
        singer nullable: true
        //author nullable: true
    }

    static mapping = {
        version(false)
        table("Album")
    }
}
