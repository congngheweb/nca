package nca

import groovy.transform.ToString
@ToString(includeNames = true)
class Song {
    String name
    String path
    Author author
    Album album
    Kind kind
    static belongsTo = [singer : Singer]

    static constraints = {
        singer nullable: true
        author nullable: true
    }

    static mapping = {
        version(false)
        table("Song")
    }
}
