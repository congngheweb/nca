package nca

/**
 * Created by dtruo on 29-Dec-15.
 */
class ArtistsController {

    def index() {
        render(view: 'artists', model: [song: Song.findById(params.id as Long)])
    }

    def contact(){
        if (params.id)
        {
            def song = Song.findAllBySinger(Singer.findById(params.id as Long))
            [songs : song]
        }
    }

}
