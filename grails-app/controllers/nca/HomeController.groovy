package nca

class HomeController {

    def index() {
        def list = Song.executeQuery(
                "select distinct sg.id, s.name, s.path, sg.image.path, s.id from Song s inner join s.singer sg"
        )
        ArrayList<Map> data = new ArrayList<>()
        if (list){
            list.each {value ->
                def map = [:]
                map.singerID = value[0]
                map.name = value[1]
                map.path = value[2]
                map.pathImage = value[3]
                map.id = value[4]
                data.add(map)
            }
        }

        render(view: 'dashboard', model : [songs : data])
    }
}
