package nca

class PlayController {

    def index() {
        render(view: 'play', model: [song: Song.findById(params.id as Long)])

    }
    def album(){
        render(view: 'playlist', model: [album: Album.findById(params.id as Long)])
    }
}
