package nca

class DetailsController {

    def details(){
        render(view: 'details', model: [details: Singer.findById(params.id as Long)])
    }

}

