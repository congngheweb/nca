package nca

class SearchController {

    def index() {}

    def wrapSearchParm(value) {
       return '%'+value+'%'
    }

    def searchsong(){
        def list = Song.findByName(wrapSearchParm(params.searchvalue))
        render(view: 'show', model: [list : list])
    }
}
