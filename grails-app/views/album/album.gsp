<%--
  Created by IntelliJ IDEA.
  User: dtruo
  Date: 29-Dec-15
  Time: 8:58 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>The Free Music-Box Website Template | Home :: w3layouts</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="layout" content="main"/>
    <script type="text/javascript">
        $(function() {
            $('#dg-container').gallery({
                autoplay	:	true
            });
        });
    </script>
</head>

<body>
<div class="content-bg">
    <div class="wrap">
        <div class="main">
            <div class="content">
                <h2>Album list</h2>
                <div class="select">
                    <select id="selectPrductSort">
                        <option value="" selected="selected">Price Drop</option>
                        <option value="">Price: lowest</option>
                        <option value="">Price: highest</option>
                        <option value="">Product Name: Z to A</option>
                        <option value="">Product Name: Z to A</option>
                        <option value="">In-stock</option>
                    </select>
                    <div class="section group example">
                        <g:each in="${nca.Album.getAll()}">
                            <div class="grid_1_of_2 images_1_of_2">
                                <a href="${createLink(controller: 'play', action: 'album', id: it.id)}"><img src="${it.image.path}" alt=""/> </a>
                                <a href="${createLink(controller: 'play', action: 'album', id: it.id)}"><h5>${it.name}</h5></a>
                            </div>
                        </g:each>

                    </div>
                </div>
            </div>
            <div class="sidebar">
                <div class="sidebar-list">
                    <h2>Categories</h2>
                    <ul>
                        <g:each in="${nca.Album.getAll()}">
                            <li><a href="${createLink(controller: 'play', action: 'album', id: it.id)}">${it.name}</a></li>
                        </g:each>
                    </ul>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
</body>
</html>