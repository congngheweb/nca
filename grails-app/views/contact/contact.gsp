<%--
  Created by IntelliJ IDEA.
  User: dtruo
  Date: 29-Dec-15
  Time: 9:02 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>The Free Music-Box Website Template | Home :: w3layouts</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="layout" content="main"/>
    <script type="text/javascript">
        $(function() {
            $('#dg-container').gallery({
                autoplay	:	true
            });
        });
    </script>
</head>

<body>
<div class="content-bg">
    <div class="wrap">
        <div class="main">
            <div class="section group">
                <div class="col span_1_of_3">
                    <div class="contact_info">
                        <h3>Vị trí hiện tại</h3>
                        <div class="map">
                            <iframe width="100%" height="175" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3918.2598104166905!2d106.80264963920216!3d10.867833065083046!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752759b2ad1ef1%3A0xaeb535e7abacaf50!2zVHLGsOG7nW5nIMSQ4bqhaSBo4buNYyBDw7RuZyBuZ2jhu4cgVGjDtG5nIHRpbiDEkEhRRy1IQ00!5e0!3m2!1svi!2s!4v1451792174553"></iframe>
                            <br><small><a href="https://www.google.com/maps/place/Tr%C6%B0%E1%BB%9Dng+%C4%90%E1%BA%A1i+h%E1%BB%8Dc+C%C3%B4ng+ngh%E1%BB%87+Th%C3%B4ng+tin+%C4%90HQG-HCM/@10.8678331,106.8026496,17z/data=!4m2!3m1!1s0x31752759b2ad1ef1:0xaeb535e7abacaf50" style="color:#666;text-align:left;font-size:12px">View Larger Map</a></small>
                        </div>
                    </div>
                    <div class="company_address">

                    </div>
                </div>
                <div class="col col1 span_2_of_4">
                    <div class="contact-form">
                        <h3>Thông tin nhóm</h3>
                        <form>
                            <div>
                                <h3><span><label>Giảng viên hướng dẫn:</label></span></h3>
                                <span>Th.S Nguyễn Trác Thức</span>
                            </div>
                            <div>
                                <h3><span><label>Danh Sách các thành viên:</label></span></h3>
                                <span>12520875 - Hoàng Trọng Nam</span>
                                <span>12520974 - Nguyễn Văn Tiến</span>
                                <span>12520763 - Nguyễn Xuân Trường</span>
                                <span>12520762 - Đào Tiến Trường</span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
            <div class="clear"></div>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>

        </div>
    </div>
</div>
</body>
</html>