<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<!-- Head -->
<head>
	<meta charset="utf-8"/>
	<title>Nhạc Của Ai</title>
	<meta name="language" content="en"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link href="${resource(dir: 'css', file: 'style.css')}" rel="stylesheet" />
	<link href='http://fonts.googleapis.com/css?family=Julius+Sans+One' rel='stylesheet' type='text/css'>
	<!--slider-->
	<script src="${resource(dir: 'js', file: 'modernizr.custom.53451.js')}"></script>
	<script src="${resource(dir: 'js', file: 'jquery.min.js')}"></script>
	<script src="${resource(dir: 'js', file: 'jquery-slider.js')}"></script>
	<script src="${resource(dir: 'js', file: 'jquery.gallery.js')}"></script>
	<link href="${resource(dir: 'assets/css', file: 'bootstrap.min.css')}" rel="stylesheet" />
	<script src="${resource(dir: 'js', file: 'soundmanager2.js')}"></script>
	<script src="${resource(dir: 'js', file: 'bar-ui.js')}"></script>
	<link rel="stylesheet" href="${resource(dir: 'css', file: 'demo.css')}">
	<link href="${resource(dir: 'css', file: 'bar-ui.css')}" rel="stylesheet" />
	<g:layoutHead/>
</head>

<body>
<div class="loading-container">
	<div class="loader"></div>
</div>
<g:render template="/layouts/header"/>
<div class="main-container container-fluid">
	<div class="page-container">
		<div class="page-content">
			<g:layoutBody/>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function() {
		$('#dg-container').gallery({
			autoplay	:	true
		});
	});
</script>
<script src="${resource(dir: 'assets/js', file: 'bootstrap.min.js')}"></script>
<g:render template="/layouts/footer"/>

</body>
</html>
