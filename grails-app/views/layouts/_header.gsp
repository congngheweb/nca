<div class="menu-bg">
    <div class="wrap">
        <div class="menu">
            <ul class="nav">
                <li><a href="${createLink(uri: '/')}">Home</a></li>
                <li><a href="${createLink(controller: 'artists',action: 'index')}">Artists</a></li>
                <li><a href="${createLink(controller: 'album',action: 'index')}">Album</a></li>
                <li><a href="${createLink(controller: 'contact',action: 'index')}">Contact</a></li>
            </ul>
        </div>
        <div class="soc-icons">
            <ul>
                <li><a href="">Get In Touch&nbsp;</a></li>
                <li><a href=""><img src="${resource(dir: 'images', file: 'facebook.png')}" alt="" /></a></li>
                <li><a href=""><img src="${resource(dir: 'images', file: 'twitter.png')}" alt="" /></a></li>
                <li><a href=""><img src="${resource(dir: 'images',file: 'gplus.png')}" alt="" /></a></li>
            </ul>
        </div>
        <div class="clear"></div>
    </div>
</div>
<div class="header-bg">
    <div class="wrap">
        <div class="header">
            <div class="logo">
                <a href="${createLink(uri: '/')}"><img src="${resource(dir: 'images', file: 'logo.png')}" alt="" title="logo"></a>
            </div>
            <div class="foot-search pull-right">
                <form>
                    <input type="text" value="keyword here" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'keyword here';}">
                    <input type="submit" value="Search">
                </form>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>