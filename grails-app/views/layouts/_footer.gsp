<div class="footer-bg">
    <div class="wrap">
        <div class="footer">
            <div class="section group">
                <div class="grid_1_of_4 images_1_of_4">
                    <h3>About Us</h3>
                    <p>Sed bibendum suscipit posuere fringilla sagittis auctor fringilla sagittis auctor. Phasellus nulla est, rhoncus non ultrices eu, lacinia ut est. Donec sodales viverra elit. Ut a malesuada diam. Suspendisse fringilla sagittis auctor. Mauris aliquam suscipit .</p>
                </div>
                <div class="grid_1_of_4 images_1_of_4">
                    <h3>News Letter</h3>
                    <p>Sed Suspendisse fringilla sagittis auctor fringilla sagittis auctor fringilla sagittis auctor. Mauris aliquam suscipit adipiscing. Pellentesque adipiscing ultrices fringilla. Maecenas semper neque nec nulla  varius ultrices. Aenean rhoncus enim nec justo.</p>
                </div>
                <div class="grid_1_of_4 images_1_of_4">
                    <h3>Recent Posts</h3>
                    <ul class="f-nav">
                        <li><a href=""><img src="${resource(dir: 'images', file: 'facebook.png')}" alt=""/>Facebook</a></li>
                        <li><a href=""><img src="${resource(dir: 'images', file: 'twitter.png')}" alt=""/>Twitter</a></li>
                        <li><a href=""><img src="${resource(dir: 'images', file: 'gplus.png')}" alt=""/>Google+</a></li>
                    </ul>
                </div>

                <div class="grid_1_of_4 images_1_of_4">
                    <h3>Latest Music</h3>
                    <p><a href=""><img src="${resource(dir: 'images', file: 'art-pic1.jpg')}" alt=""/>Sed Suspendisse fringilla sagittis auctor. Mauris aliquam suscipit adipiscing. Pellentesque  ultrices.</a></p>
                    <p class="top"><a href=""><img src="${resource(dir: 'images', file: 'art-pic2.jpg')}" alt=""/>Sed Suspendisse fringilla sagittis auctor. Mauris aliquam suscipit adipiscing. Pellentesque  ultrices.</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="footer1-bg">
    <div class="wrap">
        <div class="copy">
            <p>© 2013 All rights Reserved | Design by &nbsp;<a href="http://w3layouts.com">W3Layouts</a></p>
        </div>
    </div>
</div>