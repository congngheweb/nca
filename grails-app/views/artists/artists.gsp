<%--
  Created by IntelliJ IDEA.
  User: dtruo
  Date: 29-Dec-15
  Time: 8:48 AM
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Box Website Template | Home :: w3layouts</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="layout" content="main"/>
    <script type="text/javascript">
        $(function() {
            $('#dg-container').gallery({
                autoplay	:	true
            });
        });
    </script>
</head>

<body>
<div class="content-bg">
    <div class="wrap">
        <div class="main">
            <div class="content">
                <h2>Our Artists</h2> <%-- bắt đầu sữa --%>
                <div class="section group example">
                    <g:each in="${nca.Singer.getAll()}">
                        <div class="grid_1_of_2 images_1_of_2">
                            <a href="${createLink(controller: 'details',action: 'details', id: it.id)}"><img src="${it.image.path}" alt="" /></a>
                            <a href="${createLink(controller: 'details',action: 'details', id: it.id)}"><h3>${it.name} </h3></a>
                        </div>
                    </g:each>
                </div>
                <%-- kết thúc sữa --%>
            </div>
            <div class="sidebar">
                <div class="sidebar-list">
                    <h2>Recent Artists</h2>
                    <ul>
                        <g:each in="${nca.Singer.getAll()}">
                            <li><a href="${createLink(controller: 'details', action: 'details', id: it.id)}">${it.name}</a></li>
                        </g:each>
                    </ul>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
</body>
</html>