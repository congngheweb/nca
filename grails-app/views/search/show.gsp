<%--
  Created by IntelliJ IDEA.
  User: dtruo
  Date: 29-Dec-15
  Time: 9:02 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>The Free Music-Box Website Template | Home :: w3layouts</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="layout" content="main"/>
    <title><g:layoutTitle default="Grails" /></title>
    <link rel="stylesheet" href="${resource(dir:'css',file:'main.css')}" />
    <link rel="shortcut icon" href="${resource(dir:'images',file:'favicon.ico')}" type="image/x-icon" />
    <g:layoutHead />
    <g:javascript library="prototype" />
    <g:javascript>
        function showSpinner(visible) {
            $('spinner').style.display = visible ? "inline" : "none";
        }
        Ajax.Responders.register({
            onLoading: function() {
                showSpinner(true);
            },
            onComplete: function() {
                if(!Ajax.activeRequestCount) showSpinner(false);
            }
        });
    </g:javascript>
</head>

<body>
<div class="content-bg">
    <div class="wrap">
        <div class="main">
            <div id="searchresults">
                <g:if test="${searchresults}">
                    <br/>
                    <h3>Search Results</h3>
                    <div class="list">
                        <table>
                            <thead>
                            <tr>
                                <g:sortableColumn property="title" title="${message(code: 'book.title.label', default: 'Title')}" />
                                <g:sortableColumn property="pages" title="${message(code: 'book.pages.label', default: 'Pages')}" />
                                <th><g:message code="book.author.label" default="Author" /></th>
                                <th><g:message code="book.publisher.label" default="Publisher" /></th>
                            </tr>
                            </thead>
                            <tbody>
                            <g:each in="${searchresults}" status="i" var="bookInstance">
                                <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                                    <td>${fieldValue(bean: bookInstance, field: "title")}</td>
                                    <td>${fieldValue(bean: bookInstance, field: "pages")}</td>
                                    <td>${fieldValue(bean: bookInstance, field: "author")}</td>
                                    <td>${fieldValue(bean: bookInstance, field: "publisher")}</td>
                                </tr>
                            </g:each>
                            </tbody>
                        </table>
                    </div>
                </g:if>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
</body>
</html>