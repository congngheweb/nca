<%@ page import="nca.Song" %>
<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
    <title>The Free Music-Box Website Template | Home :: w3layouts</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="layout" content="main"/>
    <script type="text/javascript">
        $(function() {
            $('#dg-container').gallery({
                autoplay	:	true
            });
        });
    </script>
</head>
<body>

<div class="slider-bg">
    <div class="wrap">
        <div class="container">
            <section id="dg-container" class="dg-container">
                <div class="dg-wrapper">
                    <a href="#"><img src="images/1.jpg" alt="image1" /></a>
                    <a href="#"><img src="images/2.jpg" alt="image2" /></a>
                    <a href="#"><img src="images/3.jpg" alt="image3" /></a>
                    <a href="#"><img src="images/4.jpg" alt="image4" /></a>
                    <a href="#"><img src="images/5.jpg" alt="image5" /></a>
                    <a href="#"><img src="images/6.jpg" alt="image1" /></a>
                    <a href="#"><img src="images/7.jpg" alt="image2" /></a>
                    <a href="#"><img src="images/8.jpg" alt="image3" /></a>
                    <a href="#"><img src="images/9.jpg" alt="image4" /></a>
                    <a href="#"><img src="images/10.jpg" alt="image5" /></a>
                    <a href="#"><img src="images/11.jpg" alt="image1" /></a>
                    <a href="#"><img src="images/12.jpg" alt="image2" /></a>
                    <a href="#"><img src="images/13.jpg" alt="image3" /></a>
                </div>
            </section>
        </div>
    </div>
</div>
<div class="content-bg">
    <div class="wrap">
        <div class="main">
            <div class="content">
                <h2>Bài Hát Hay</h2>
                <div class="section group">
                    <g:each in="${songs?.subList(0,5)}" var="data">
                        <div class="grid_1_of_3 images_1_of_3">
                            <a href="${createLink(controller: 'play', action: 'index', id: data.id)}"><img src="${data.pathImage}" alt=""/> </a>
                            <a href="${createLink(controller: 'play', action: 'index', id: data.id)}"><h3>${data.name}</h3></a>

                        </div>
                    </g:each>

                </div>
                <h2 class="bg">Nghe Nhiều</h2>
                <div class="section group">
                    <div class="section group">
                        <g:each in="${songs?.subList(6,11)}" var="data" status="idx">
                            <div class="grid_1_of_3 images_1_of_3">
                                <a href="${createLink(controller: 'play', action: 'index', id: data.id)}"><img src="${data.pathImage}" alt=""/> </a>
                                <a href="${createLink(controller: 'play', action: 'index', id: data.id)}"><h3>${data.name}</h3></a>

                            </div>
                        </g:each>

                    </div>
                </div>
            </div>
            <div class="sidebar">
                <div class="sidebar-list">
                    <h2>Bài Hát Sắp Hot</h2>
                    <ul>
                        <g:each in="${songs?.subList(0,15)}" var="data" status="idx">
                            <li><a href="${createLink(controller: 'play', action: 'index', id: data.id)}">${data.name} </a></li>
                        </g:each>
                    </ul>

                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
</body>
</html>