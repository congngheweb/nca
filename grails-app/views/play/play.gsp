<%@ page import="nca.Image; nca.Singer" %>
<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
    <title>The Free Music-Box Website Template | Details :: w3layouts</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="layout" content="main"/>

</head>
<body>
<div class="content-bg">
    <div class="wrap">
        <div class="main">
            <div class="content">
                <h2>${song.name}</h2>
                <div class="section group">
                    <div class="cont">
                        <div class="single">
                            <div class="grid-img1">
                                <img src="${song.singer.image.path}" alt="">
                            </div>
                            <div class="para">
                                <p><h4>${song.name} - ${song.singer.name}</h4></p>
                                <p><h5>Sáng tác: ${song.author.name}</h5></p>
                            </div>
                            <div class="sm2-bar-ui full-width">

                                <div class="bd sm2-main-controls">

                                    <div class="sm2-inline-texture"></div>
                                    <div class="sm2-inline-gradient"></div>

                                    <div class="sm2-inline-element sm2-button-element">
                                        <div class="sm2-button-bd">
                                            <a href="#play" class="sm2-inline-button play-pause">Play / pause</a>
                                        </div>
                                    </div>

                                    <div class="sm2-inline-element sm2-inline-status">

                                        <div class="sm2-playlist">
                                            <div class="sm2-playlist-target">
                                                <!-- playlist <ul> + <li> markup will be injected here -->
                                                <!-- if you want default / non-JS content, you can put that here. -->
                                                <noscript><p>JavaScript is required.</p></noscript>
                                            </div>
                                        </div>

                                        <div class="sm2-progress">
                                            <div class="sm2-row">
                                                <div class="sm2-inline-time">0:00</div>
                                                <div class="sm2-progress-bd">
                                                    <div class="sm2-progress-track">
                                                        <div class="sm2-progress-bar"></div>
                                                        <div class="sm2-progress-ball"><div class="icon-overlay"></div></div>
                                                    </div>
                                                </div>
                                                <div class="sm2-inline-duration">0:00</div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="sm2-inline-element sm2-button-element sm2-volume">
                                        <div class="sm2-button-bd">
                                            <span class="sm2-inline-button sm2-volume-control volume-shade"></span>
                                            <a href="#volume" class="sm2-inline-button sm2-volume-control">volume</a>
                                        </div>
                                    </div>

                                </div>

                                <div class="bd sm2-playlist-drawer sm2-element">

                                    <div class="sm2-inline-texture">
                                        <div class="sm2-box-shadow"></div>
                                    </div>

                                    <!-- playlist content is mirrored here -->

                                    <div class="sm2-playlist-wrapper">
                                        <ul class="sm2-playlist-bd">
                                            <li><a href="${song.path}">${song.name}</a></li>
                                        </ul>
                                    </div>

                                </div>

                            </div>
                            <div class="clear"></div>
                            <div>
                                <a href="${song.path}" download id="download" >Download</a>
                            </div>
                            %{--<script type="text/javascript">--}%
                            %{--document.getElementById('download').onclick();--}%
                            %{--</script>--}%
                        </div>
                        <div class="text-h1 top">
                            <h2>Album Liên Quan</h2>
                        </div>
                        <div class="div2">
                            <div id="mcts1">
                                <g:each in="${nca.Album.getAll()}">
                                    <img src="${it.image.path}" alt=""/>
                                </g:each>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sidebar">
                <div class="sidebar-list">
                    <h2>Categories</h2>
                    <ul>
                        <g:each in="${nca.Song.findAllBySinger(nca.Singer.findById(song.singerId))}">
                            <li><a href="${createLink(controller: 'play', action: 'index', id : it.id)}">${it.name} </a></li>
                        </g:each>
                    </ul>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
</body>
</html>