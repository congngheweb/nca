<%--
  Created by IntelliJ IDEA.
  User: dtruo
  Date: 29-Dec-15
  Time: 5:00 PM
--%>

<%@ page import="nca.Singer" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>The Free Music-Box Website Template | Home :: w3layouts</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="layout" content="main"/>
    <script type="text/javascript">
        $(function() {
            $('#dg-container').gallery({
                autoplay	:	true
            });
        });
    </script>
</head>

<body>

<div class="content-bg">
    <div class="wrap">
        <div class="main">
            <div class="content">
                <h2>SONG LIST'S ${details.name}</h2>
                <div class="section group">
                    <div class="cont">
                        <div class="single">
                            <g:each in="${nca.Song.findAllBySinger(details)}">
                                <a href="${createLink(controller: 'play', action: 'index', id: it.id)}"><h5>►  ${it.name} - ${it.singer.name}</h5></a>
                            </g:each>
                            <div class="clear"></div>
                        </div>
                        <div class="text-h1 top">
                            <h2>ALBUM</h2>
                        </div>
                        <div class="div2">
                            <div id="mcts1">
                                <g:each in="${nca.Album.getAll()}">
                                    <div class="item" style="display: block; float: left;">
                                        <a href="${createLink(controller: 'play', action: 'album', id: it.id)}"><img src="${it.image.path}" alt=""/> </a>
                                    </div>
                                </g:each>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sidebar">
                <div class="sidebar-list">
                    <h2>Recent Singer</h2>
                    <ul>
                        <g:each in="${nca.Singer.getAll()}">
                            <li><a href="${createLink(controller: 'details', action: 'details', id: it.id)}">${it.name}</a></li>
                        </g:each>
                    </ul>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
</body>
</html>